import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  MyApp createState() => MyApp();
}

void main() => runApp(MainPage());

class MyApp extends State<MainPage> {
  var colorButton1 = Colors.grey;
  var colorButton2 = Colors.grey;
  String sex = "Male";
  String height = "";
  String weight = "";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Builder(builder: (context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Calculo IMC'),
            backgroundColor: Colors.green,
            actions: [
              IconButton(
                onPressed: () {
                  setState(() {
                    colorButton1 = Colors.grey;
                    colorButton2 = Colors.grey;
                    sex = "Male";
                    height = "0.0";
                    weight = "0.0";
                  });
                },
                icon: Icon(Icons.delete_forever),
              ),
            ],
          ),
          body: Center(
            child: Padding(
              padding: EdgeInsets.only(top: 30, left: 10, right: 10),
              child: Column(children: [
                Text(
                  'Ingrese sus datos para calcular el IMC',
                  style: TextStyle(fontSize: 17),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            sex = "Female";
                            colorButton1 = Colors.blue;
                            colorButton2 = Colors.grey;
                          });
                        },
                        icon: Icon(
                          Icons.female,
                          color: colorButton1,
                        )),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            sex = "Male";
                            colorButton2 = Colors.blue;
                            colorButton1 = Colors.grey;
                          });
                        },
                        icon: Icon(
                          Icons.male,
                          color: colorButton2,
                        ))
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, top: 20),
                  child: TextField(
                    onChanged: (value) => setState(() {
                      height = value;
                    }),
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(
                        Icons.square_foot_rounded,
                      ),
                      border: OutlineInputBorder(),
                      labelText: 'Ingresar Altura (metros)',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, top: 20),
                  child: TextField(
                    onChanged: (value) => setState(() {
                      weight = value;
                    }),
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(
                        Icons.monitor_weight,
                      ),
                      border: OutlineInputBorder(),
                      labelText: 'Ingresar Peso (KG)',
                    ),
                  ),
                ),
                TextButton(
                    onPressed: () {
                      showAlertDialog(context, sex, weight, height);
                    },
                    child: Text(
                      "Calcular",
                      style: TextStyle(color: Colors.black),
                    )),
              ]),
            ),
          ),
        );
      }),
    );
  }
}

showAlertDialog(BuildContext context, sex, weight, height) {
  double imc = 0.0;
  if (height != "" && weight != "")
    imc = double.parse(getIMC(weight, height).toStringAsFixed(2));

  Widget okButton = TextButton(
    child: Text(
      "Aceptar",
      style: TextStyle(color: Colors.black),
    ),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Tu Indice de Masa Corporal Es: ${imc}"),
    content: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        sex == "Female"
            ? Text("Tabla del IMC para Mujeres")
            : Text("Tabla del IMC para Hombres"),
        Text("Edad    Peso normal/IMC ideal"),
        sex == "Female" ? Text("16-17\t19-24") : Text("16\t19-24"),
        sex == "Female" ? Text("18\t19-24") : Text("17\t20-25"),
        sex == "Female" ? Text("19-24\t19-24") : Text("18\t21-26"),
        sex == "Female" ? Text("25-34\t20-25") : Text("19-24\t22-27"),
        sex == "Female" ? Text("35-44\t21-26") : Text("25-34\t23-38"),
      ],
    ),
    actions: [
      okButton,
    ],
  );
  AlertDialog alert2 = AlertDialog(
    title: Text("No se proporcionaron los datos necesarios"),
    actions: [
      okButton,
    ],
  );
  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      if (height != "" && weight != "")
        return alert;
      else
        return alert2;
    },
  );
}

double getIMC(kg, m) {
  return double.parse(kg) / (double.parse(m) * double.parse(m));
}
